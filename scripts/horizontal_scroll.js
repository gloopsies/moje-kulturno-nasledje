const scroll_horizontally = (e, element) => {
  e = window.event || e;
  let delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));
  element.scrollLeft -= delta * 300;
  if (element.scrollLeft != 0) {
    e.preventDefault();
  }
};

// prettier-ignore
const apply_scroll = (element) => {
  if (element.addEventListener) {
    element.addEventListener("mousewheel", (e) => scroll_horizontally(e, element), false);
    element.addEventListener("DOMMouseScroll", (e) => scroll_horizontally(e, element), false);
  } else {
    element.attachEvent("onmousewheel", (e) => scroll_horizontally(e, element));
  }
};
