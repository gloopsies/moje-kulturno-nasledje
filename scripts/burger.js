let opened = false;

const burger_click = () => {
  nav_bar.classList.toggle("active");
  bg_element.classList.toggle("menu");
  opened = !opened;
  if (opened) {
    bg_element.onclick = () => burger_click();
  } else {
    bg_element.onclick = null;
  }
};

const burger_close = () => {
  nav_bar.classList.remove("active");
  bg_element.classList.remove("menu");
  opened = false;
  bg_element.onclick = null;
};
