const root_folder = "scripts/";
let scripts = [
  "burger.js",
  "horizontal_scroll.js",
  "shuffle.js",
  "bastine.js",
  "load.js",
];

let current_index = 0;

let loadscripts = () => {
  var imported = document.createElement("script");
  imported.src = root_folder + scripts[current_index++];
  document.head.appendChild(imported);
  imported.onload = () => {
    if (current_index < scripts.length) loadscripts();
  };
};

loadscripts();
