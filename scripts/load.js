let landing;
let kulturna_dobra;
let spisak_bastina;
let predlozeni_spisak;
let nematerijalni_spisak;
let bg_element;
let about;
let nav_bar;
let burger;

let view_element = null;
let viewed_element = null;
let image_no = 0;
let slideshow = false;
let imgs = [];

const bastina_click = (el, bastina, src) => {
  kulturna_dobra.scrollIntoView();

  bg_element.classList.add("focus");
  let iHTML = `<h2>${bastina.naziv}</h2><div>`;
  bastina.opis.forEach((element) => {
    iHTML += `<p>${element}</p>`;
  });
  iHTML += `</div>`;
  about.innerHTML = iHTML;
  viewed_element = el;
  view_element = viewed_element.firstChild.cloneNode(true);
  const bound_box = viewed_element.getBoundingClientRect();
  view_element.style.setProperty("--top", bound_box.top + "px");
  view_element.style.setProperty("--left", bound_box.left - 5 + "px");
  view_element.style.setProperty("--width", bound_box.width + "px");
  view_element.style.setProperty("--height", bound_box.height + "px");

  view_element.classList.add("full-view");

  setTimeout(() => {
    view_element.classList.add("scale-up");
  }, 1);

  let img2_div;
  let img2_img1;
  let img2_img2;

  img2_div = document.createElement("div");
  img2_div.classList.add("scale-up");
  img2_div.classList.add("slideshow_right");
  img2_img1 = document.createElement("img");
  img2_img1.src = src + bastina.folder + "/sl2.jpg";
  img2_img1.classList.add("slideshow");
  img2_img1.style.objectFit = "cover";
  img2_img1.style.filter = "blur(10px)";
  img2_div.appendChild(img2_img1);
  img2_img2 = document.createElement("img");
  img2_img2.src = src + bastina.folder + "/sl2.jpg";
  img2_img2.classList.add("slideshow");
  img2_img2.style.objectFit = "scale-down";
  img2_div.appendChild(img2_img2);
  bg_element.appendChild(img2_div);

  img3_div = document.createElement("div");
  img3_div.classList.add("scale-up");
  img3_div.classList.add("slideshow_right");
  img3_img1 = document.createElement("img");
  img3_img1.src = src + bastina.folder + "/sl3.jpg";
  img3_img1.classList.add("slideshow");
  img3_img1.style.objectFit = "cover";
  img3_img1.style.filter = "blur(10px)";
  img3_div.appendChild(img3_img1);
  img3_img2 = document.createElement("img");
  img3_img2.src = src + bastina.folder + "/sl3.jpg";
  img3_img2.classList.add("slideshow");
  img3_img2.style.objectFit = "scale-down";
  img3_div.appendChild(img3_img2);
  bg_element.appendChild(img3_div);

  image_no = 0;
  imgs = [view_element, img2_div, img3_div];

  bg_element.onclick = () => {
    img2_div.remove();
    img3_div.remove();
    view_element.classList.remove("scale-up");
    bg_element.classList.remove("focus");
    imgs = [];
    setTimeout(() => {
      view_element.remove();
      document.body.classList.remove("opened");
    }, 500);
  };

  bg_element.appendChild(view_element);
  document.body.classList.add("opened");
};

const sledeca_slika = (e) => (e + 1) % 3;
const prosla_slika = (e) => (e + 2) % 3;

const slika_levo = () => {
  imgs[image_no].classList.add("slideshow_right");
  image_no = prosla_slika(image_no);
  setTimeout(() => {
    imgs[sledeca_slika(image_no)].classList.add("slideshow_left");
    imgs[sledeca_slika(image_no)].classList.remove("slideshow_right");
  }, 200);

  imgs[image_no].classList.remove("slideshow_right");
  imgs[image_no].classList.add("slideshow_left");
  setTimeout(() => {
    imgs[image_no].classList.remove("slideshow_left");
  }, 1);
};

const slika_desno = () => {
  imgs[image_no].classList.add("slideshow_left");
  image_no = sledeca_slika(image_no);
  setTimeout(() => {
    imgs[prosla_slika(image_no)].classList.add("slideshow_right");
    imgs[prosla_slika(image_no)].classList.remove("slideshow_left");
  }, 200);

  imgs[image_no].classList.remove("slideshow_left");
  imgs[image_no].classList.add("slideshow_right");
  setTimeout(() => {
    imgs[image_no].classList.remove("slideshow_right");
  }, 1);
};

const dodaj_bastinu = (element, container, src) => {
  let div = document.createElement("div");
  div.classList.add("bastina");
  div.onclick = () => bastina_click(div, element, src);
  let img = document.createElement("img");
  img.loading = "lazy";
  img.src = src + element.folder + "/sl1.jpg";
  img.alt = element.naziv;
  img.width = 400;
  img.height = 300;
  let h3 = document.createElement("h3");
  h3.textContent = element.naziv;

  div.appendChild(img);
  div.appendChild(h3);

  container.appendChild(div);
};

const load = () => {
  landing = document.getElementById("landing");
  spisak_bastina = document.getElementById("spisak");
  predlozeni_spisak = document.getElementById("predlozeni-spisak");
  nematerijalni_spisak = document.getElementById("nematerijalni-spisak");
  bg_element = document.getElementById("bg-element");
  kulturna_dobra = document.getElementById("spisak-bastina");
  about = bg_element.querySelector(".about");
  nav_bar = document.getElementById("nav");
  burger = document.getElementById("burger");
  burger.onclick = () => burger_click();

  shuffle(bastine.bastine);
  shuffle(predlozene_bastine.bastine);

  for (let el of bastine.bastine) {
    dodaj_bastinu(el, spisak_bastina, bastine.src);
  }

  for (let el of predlozene_bastine.bastine) {
    dodaj_bastinu(el, predlozeni_spisak, predlozene_bastine.src);
  }

  for (let el of nematerijalna_dobra.bastine) {
    dodaj_bastinu(el, nematerijalni_spisak, nematerijalna_dobra.src);
  }

  landing.style.backgroundImage = `url(${bastine.src}${bastine.bastine[0].folder}/sl1.jpg)`;
  let bastina = document.createElement("div");
  bastina.innerHTML = `
    <p>${bastine.bastine[0].naziv}</p>
    <button onclick="bastina_click(spisak_bastina.children[0], bastine.bastine[0])">
      Procitaj jos
    </button>
  `;
  landing.appendChild(bastina);

  apply_scroll(spisak_bastina);
  apply_scroll(predlozeni_spisak);
  apply_scroll(nematerijalni_spisak);

  let levo = document.querySelector(".levo");
  let desno = document.querySelector(".desno");

  levo.onclick = (e) => {
    slika_levo();
    e.stopPropagation();
  };
  desno.onclick = (e) => {
    slika_desno();
    e.stopPropagation();
  };
};

load();
